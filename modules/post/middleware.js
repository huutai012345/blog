import Joi from '@hapi/joi';
import { errorRequest } from '../../library/api/index.js';

export const validateCreatePost = (req, res, next) => {
  const schema = Joi.object({
    title: Joi.string().max(100).required(),
    content: Joi.string().min(10).required(),
    author_id: Joi.string().min(10).required(),
  });

  schema
    .validateAsync(req.body, { abortEarly: false })
    .then(() => next())
    .catch((err) => {
      errorRequest(req, res, err);
    });
};

export const validateUpdatePost = (req, res, next) => {
  const schema = Joi.object({
    title: Joi.string().max(100).required(),
    content: Joi.string().min(10).required(),
  });

  schema
    .validateAsync(req.body, { abortEarly: false })
    .then(() => next())
    .catch((err) => {
      errorRequest(req, res, err);
    });
};
