import Post from '../../models/Post.js';
import User from '../../models/User.js';
import { errorRequest, outputSuccess } from '../../library/api/index.js';
import ErrorMessage from '../../configs/ErrorMessage.js';
import Utils from '../../library/utils/index.js';
import Constant from '../../configs/Constant.js';

export const createPost = async (req, res) => {
  const { title, author_id, content } = req.body;

  try {
    const author = await User.findOne({ _id: author_id }).exec();
    if (!author) {
      return errorRequest(req, res, ErrorMessage.USER_NOT_FOUND);
    }

    let now =Date.now();
    Post.init(); // <- document gets generated

    const post = new Post({
      title,
      author,
      content,
      created_at: now,
      updated_at: now,
    });

    const doc = await post.save();

    return outputSuccess(req, res, {
      _id: doc._id,
      title: doc.title,
      content: doc.content,
      author_id: doc.author._id,
    });
  } catch (error) {
    return errorRequest(req, res, error);
  }
};

export const getPost = async (req, res) => {
  const id = req.params.id;

  await Post.findById(id)
    .populate({ path: 'author', select: ['name', 'email'] })
    .exec((err, result) => {
      if (err) {
        return errorRequest(req, res, err);
      }
      if (!result) {
        return errorRequest(req, res, ErrorMessage.POST_NOT_FOUND);
      }
      return outputSuccess(req, res, result);
    });
};

export const updatePost = async (req, res) => {
  const user = req.user;
  const id = req.params.id;
  const { title, content } = req.body;
  const options = { new: true };

  await Post.findOneAndUpdate({ _id: id, author: user._id }, { title, content }, options)
    .populate({ path: 'author', select: ['name', 'email'] })
    .exec((err, result) => {
      if (err) {
        return errorRequest(req, res, ErrorMessage.POST_NOT_FOUND);
      }
      return outputSuccess(req, res, result);
    });
};

export const deletePost = async (req, res) => {
  const id = req.params.id;
  const user = req.user;

  try {
    const post = await Post.deleteOne({ _id: id, author: user._id }).exec();
    if (post.deletedCount > 0) return outputSuccess(req, res, { id });
    else return errorRequest(req, res, ErrorMessage.POST_NOT_FOUND);
  } catch (error) {
    return errorRequest(req, res, error);
  }
};

export const getListPost = async (req, res) => {
  const query = req.query;

  const page = query.page ?? Constant.PAGE_DEFAULT;
  const pageSize = query.page_size ?? Constant.PAGE_SIZE_DEFAULT;

  let builder = Post.find();

  // get list by any conditions here

  // end where conditions

  const pagination = await Utils.pagination(builder, page, pageSize);

  return outputSuccess(req, res, pagination);
};
