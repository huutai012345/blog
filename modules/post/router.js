import express from 'express';
import { createPost, getPost, updatePost, deletePost, getListPost } from './handle.js';
import { validateCreatePost, validateUpdatePost } from './middleware.js';
import { validAuth, validRole } from '../user/middleware.js';

const postRouter = express.Router();

/*
 @desc      Create new Post
 @route     POST /api/v1/post
 @access    Public
*/
postRouter.post('/', validateCreatePost, createPost);

/*
 @desc      Get post detail
 @route     GET /api/v1/post/:id
 @access    Public
*/
postRouter.get('/:id', getPost);

/*
 @desc      Update post
 @route     PUT /api/v1/post/update/:id
 @access    Public
*/
postRouter.put('/update/:id', [validAuth, validRole, validateUpdatePost], updatePost);

/*
 @desc      Delete post
 @route     DELETE /api/v1/post/delete/:id
 @access    Public
*/
postRouter.delete('/delete/:id', [validAuth, validRole], deletePost);

/*
@desc       Get list post
@route     GET /api/v1/post
@access    Public
*/
postRouter.get('/', getListPost);

export default postRouter;
