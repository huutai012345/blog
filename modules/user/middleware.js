import Joi from '@hapi/joi';
import { errorRequest, unAuthorized, tokenExpired } from '../../library/api/index.js';
import User from '../../models/User.js';
import jwt from '../../library/jwt/index.js';
import Constant from '../../configs/Constant.js';

export const validateRegister = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email({ tlds: { allow: false } }),
    name: Joi.string().required(),
    password: Joi.string().min(6).max(15).required(),
    password_confirm: Joi.string().valid(Joi.ref('password')).required(),
  });

  schema
    .validateAsync(req.body, { abortEarly: false })
    .then(() => next())
    .catch((err) => {
      errorRequest(req, res, err);
    });
};

export const validateLogin = (req, _, next) => {
  const schema = Joi.object({
    email: Joi.string().email({ tlds: { allow: false } }),
    password: Joi.string().required(),
  });

  //abortEarly: when true, warnings are returned alongside the value (i.e. `{ value, warning }`).
  schema
    .validateAsync(req.body, { abortEarly: false })
    .then(() => next())
    .catch((err) => {
      errorRequest(req, _, err);
    });
};

export const validAuth = (req, _, next) => {
  let token = req.headers['x-access-token'] || req.headers['authorization'];

  if (token && token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
    try {
      const payload = jwt.verifyAccessToken(token);
      User.findById(payload._id)
        .populate('role')
        .then((user) => {
          req.user = user;
          next();
        });
    } catch (error) {
      unAuthorized(req, _);
    }
  } else unAuthorized(req, _);
};

export const validRole = (req, res, next) => {
  const user = req.user;

  if (user.role.role_name == Constant.USER_ROLE_GUEST) {
    next();
  } else unAuthorized(req, res);
};

export const validateResetPassword = (req, res, next) => {
  const schema = Joi.object({
    password: Joi.string().min(6).max(15).required(),
    password_confirm: Joi.string().valid(Joi.ref('password')).required(),
  });
  schema
    .validateAsync(req.body, { abortEarly: false })
    .then(() => next())
    .catch((err) => {
      errorRequest(req, res, err);
    });
};

export const validateChangePassword = (req, res, next) => {
  const schema = Joi.object({
    password: Joi.string().min(6).max(15).required(),
    new_password: Joi.string().invalid(Joi.ref('password')).min(6).max(15).required(),
    new_password_confirm: Joi.string().valid(Joi.ref('new_password')).required(),
  });
  schema
    .validateAsync(req.body, { abortEarly: false })
    .then(() => next())
    .catch((err) => {
      errorRequest(req, res, err);
    });
};

export const validUpdateProfile = (req, _, next) => {
  Joi.object({
    name: Joi.string().required(),
  })
    .validateAsync(req.body, { abortEarly: true })
    .then(() => next())
    .catch((error) => {
      errorRequest(req, _, error);
    });
};

export const validateRefreshToken = (req, _, next) => {
  Joi.object({
    refresh_token: Joi.string().required(),
  })
    .validateAsync(req.body, { abortEarly: true })
    .then(() => next())
    .catch((error) => {
      errorRequest(req, _, error);
    });
};
