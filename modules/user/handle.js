import User from '../../models/User.js';
import ErrorMessage from '../../configs/ErrorMessage.js';
import { outputSuccess, errorRequest } from '../../library/api/index.js';
import jwt from '../../library/jwt/index.js';
import sgMail from '../../library/sendgrid/index.js';
import Utils from '../../library/utils/index.js';
import client from '../../library/redis/index.js';
import Constant from '../../configs/Constant.js';
import Role from '../../models/role.js';

export const registerAction = async (req, res) => {
  const { email, password, name } = req.body;

  const user = await User.findOne({ email }).exec();

  if (user) {
    return errorRequest(req, res, ErrorMessage.USER_EXISTS);
  }

  User.init(); // <- document gets generated

  let now = Date.now();
  const newUser = new User({
    name,
    password: await Utils.makePassword(password),
    email,
    token: Utils.randomToken(),
    created_at: now,
    updated_at: now,
  });

  newUser.save((err, user) => {
    if (err) {
      return errorRequest(req, res, err);
    }

    const role_name = Constant.USER_ROLE_GUEST;

    Role.findOne({ role_name }, (err, role) => {
      if (err) {
        return errorRequest(req, res, err);
      }

      user.role = role;

      user.save((err) => {
        if (err) {
          return errorRequest(req, res, err);
        }

        return outputSuccess(req, res, user);
      });
    });
  });
};

export const loginAction = async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email }).populate('role').exec();

  if (!user) {
    return errorRequest(req, res, ErrorMessage.USER_NOT_FOUND);
  }

  if (await Utils.comparePassword(password, user.password)) {
    const accessToken = jwt.signAccessToken({
      _id: user._id,
      name: user.name,
      email: user.email,
      role: user.role.code,
    });

    const refreshToken = jwt.signRefreshToken({
      _id: user._id,
    });

    console.log(user._id);

    client.set(user._id.toString(), refreshToken);

    console.log('\n');
    console.log('TOKEN CHECK: ', accessToken);
    console.log('\n');

    return outputSuccess(req, res, {
      accessToken,
      refreshToken,
      user_info: user,
    });
  }
  return errorRequest(req, res, ErrorMessage.PASSWORD_NOT_ALLOWED);
};

export const forgotPassword = async (req, res) => {
  const { email } = req.body;

  try {
    const user = await User.findOne({ email }).exec();
    if (!user) {
      return errorRequest(req, res, ErrorMessage.USER_NOT_FOUND);
    }

    const html = `<a href="${req.protocol}://localhost:3000/UpdatePassword?
                      email=${user.email}&token=${user.token}">Click me</a>`;
    await sgMail.send(email, 'Reset Password', 'Please click link to reset password ', html);
    return outputSuccess(req, res, { id: user.id });
  } catch (error) {
    return errorRequest(req, res, error);
  }
};

export const updatePassword = async (req, res) => {
  const { email, token } = req.query;
  const { password } = req.body;
  const options = { new: true };

  try {
    const user = await User.findOneAndUpdate(
      { email, token },
      { password: await Utils.makePassword(password), token: Utils.randomToken() },
      options
    ).exec();

    if (!user) {
      return errorRequest(req, res, ErrorMessage.USER_EXISTS);
    }
    return outputSuccess(req, res, { email: user.email, token: user.token });
  } catch (error) {
    return errorRequest(req, res, error);
  }
};

export const changePassword = async (req, res) => {
  const { password, new_password } = req.body;
  const user = req.user;
  if (await Utils.comparePassword(password, user.password)) {
    await User.findOneAndUpdate({ _id: user._id }, { password: await Utils.makePassword(new_password) }).exec();
    return outputSuccess(req, res, { _id: user._id });
  }
  return errorRequest(req, res, ErrorMessage.PASSWORD_NOT_ALLOWED);
};

export const logoutAction = async (req, res) => {
  const _id = req.user._id;
  const user = await User.findOne({ _id }).populate('role').exec();

  if (!user || user.role.code !== Constant.USER_ROLE_GUEST) {
    return errorRequest(req, res, ErrorMessage.UNAUTHORIZED);
  }
  req.user = null;
  return outputSuccess(req, res, { _id });
};

export const updateProfileAction = async (req, res) => {
  const _id = req.user._id;

  const update = Object.assign(req.body, {
    updated_at: Date.now(),
  });

  const options = {
    new: true,
  };
  console.log(update);
  User.findByIdAndUpdate(_id, update, options, (err, user) => {
    if (err) {
      return errorRequest(req, res, err);
    }

    return outputSuccess(req, res, { _id });
  });
};

export const refreshToken = (req, res) => {
  const { refresh_token } = req.body;

  try {
    const payload = jwt.verifyRefreshToken(refresh_token);

    User.findById(payload._id).then(async (user) => {
      const result = await client.get(payload._id);
      if (refresh_token === result) {
        const accessToken = jwt.signAccessToken({
          _id: user._id,
          name: user.name,
          email: user.email,
          role: user.role.code,
        });

        const refreshToken = jwt.signRefreshToken({
          _id: user._id,
        });

        client.set(user._id.toString(), refreshToken);
        return outputSuccess(req, res, { accessToken, refreshToken });
      }
      return errorRequest(req, res, ErrorMessage.USER_NOT_FOUND);
    });
  } catch (error) {
    errorRequest(req, res, error);
  }
};
