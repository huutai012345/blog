import express from 'express';
import {
  registerAction,
  loginAction,
  forgotPassword,
  updatePassword,
  changePassword,
  logoutAction,
  updateProfileAction,
  refreshToken,
} from './handle.js';
import {
  validateRegister,
  validateLogin,
  validateResetPassword,
  validateChangePassword,
  validAuth,
  validUpdateProfile,
  validateRefreshToken,
} from './middleware.js';

const userRouter = express.Router();

/*
 @desc      Create new User
 @route     POST /api/v1/auth/register
 @access    Public
*/
userRouter.post('/register', validateRegister, registerAction);

/*
    @desc   Login with email and password
    @route  POST /api/v1/auth/login
    @access Public
*/
userRouter.post('/login', validateLogin, loginAction);

/*
    @desc   Forgot Password
    @route  POST /api/v1/auth/forgot-password
    @access Public
*/
userRouter.post('/forgot-password', forgotPassword);

/*
    @desc   Forgot Password
    @route  POST /api/v1/auth/reset-password
    @access Public
*/
userRouter.put('/update-password', validateResetPassword, updatePassword);

/*
    @desc   Change Password
    @route  POST /api/v1/auth/change-password
    @access Public
*/
userRouter.put('/change-password', [validAuth, validateChangePassword], changePassword);

/*
    @desc   Api Logout
    @route  POST /api/v1/auth/logout
    @access private
*/
userRouter.post('/logout', validAuth, logoutAction);

/*
    @desc update user profile
    @route  POST /api/v1/auth/me/update
    @access private
*/
userRouter.post('/me/update', [validAuth, validUpdateProfile], updateProfileAction);

/*
    @desc   Refresh token
    @route  POST /api/v1/auth/refresh-token
    @access Public
*/
userRouter.post('/refresh-token', validateRefreshToken, refreshToken);

export default userRouter;
