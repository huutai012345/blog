import express from 'express';
import morgan from 'morgan';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cors from 'cors';
import swaggerUi from 'swagger-ui-express';
import userRouter from './modules/user/router.js';
import postRouter from './modules/post/router.js';
import swaggerDocument from './swagger.json';
const app = express();

//  Middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Connect to MongoDB
const connectionString =
  'mongodb+srv://beblog:myPassword@cluster0.jvxoi.gcp.mongodb.net/sample_blog?retryWrites=true&w=majority';

mongoose
  .connect(connectionString, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: true,
  })
  .then(() => {
    console.log('Database connected');
  })
  .catch((err) => console.log('Error connected: ', err));

// Routes
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1/auth', userRouter);
app.use('/api/v1/post', postRouter);

// Server
const port = process.env.PORT;
app.listen(port, () => {
  console.log(`Server is running on port: ${port}`.cyan);
});
