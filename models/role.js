import mongoose from 'mongoose';

const Role = new mongoose.Schema({
  role_name: {
    type: String,
    required: true,
  },
  role_desc: {
    type: String,
    required: false,
  },
  created_at: {
    type: Date,
    required: false,
  },
  updated_at: {
    type: Date,
    required: false,
  },
});

export default mongoose.model('role', Role);
