import mongoose from 'mongoose';

const User = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: { type: mongoose.Schema.Types.ObjectId, ref: 'role' },
  token: {
    type: String,
    required: false,
  },
  created_at: {
    type: Date,
    required: false,
  },
  created_by: {
    type: String,
    required: false,
  },
  updated_at: {
    type: Date,
    required: false,
  },
  updated_by: {
    type: String,
    required: false,
  },
});

export default mongoose.model('user', User);
