class ErrorMessage {
  static USER_EXISTS = 'User is already exist';
  static USER_NOT_FOUND = 'User is not found';
  static USER_NOT_LOGIN = 'User is not login';
  static POST_NOT_FOUND = 'Post is not found';
  static PASSWORD_NOT_ALLOWED = 'Password is incorrect';
  static UNAUTHORIZED = 'Unauthorized';
}

export default ErrorMessage;
