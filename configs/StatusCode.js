class StatusCode {
  static SUCCESS = 200;
  static NOT_FOUND = 404;
  static FORBIDDEN = 403;
  static UNAUTH = 401;
  static BAD_REQUEST = 400;
  static CONTENT_NOT_ALLOWED = 1009;
}

export default StatusCode;
