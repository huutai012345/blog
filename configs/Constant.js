class Constant {
  static MESSAGE_CONTENT_DEFAULT = 'Successfully';
  static MESSAGE_CONTENT_ERROR = 'Bad Request';

  static USER_ROLE_ADMIN = 'admin';
  static USER_ROLE_GUEST = 'guest';

  static PAGE_DEFAULT = 1;
  static PAGE_SIZE_DEFAULT = 10;
}

export default Constant;
