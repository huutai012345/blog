import redis from 'redis';

const client = redis.createClient({
  port: 6379,
  host: '127.0.0.1',
});

const expiresIn = 7 * 24 * 60 * 60;

client.on('connect', function (error) {
  console.log('Client connected to redis');
});

client.on('error', function (error) {
  console.error(error);
});

client.on('error', function (error) {
  console.error(error);
});

client.on('end', function (error) {
  console.log('Client disconnected');
});

const set = (key, value) => {
  return client.set(key, value, 'EX', expiresIn, (err, result) => {
    if (err) {
      return error;
    }
    return result;
  });
};

const get = (key) => {
  return new Promise((resolve, reject) => {
    client.get(key, (err, result) => {
      if (err !== null) {
        reject(err);
      }
      resolve(result);
    });
  });
};

export default { set, get };
