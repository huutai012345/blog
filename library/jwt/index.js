import jwt from 'jsonwebtoken';

const privateKey = String(process.env.JWT_SECRET_KEY);

const signAccessToken = (payload) => {
  return jwt.sign(payload, privateKey, {
    issuer: 'BEBLOG',
    subject: 'quockidang@gmail.com',
    audience: 'quockidang@gmail.com',
    expiresIn: '120s',
  });
};

const signRefreshToken = (payload) => {
  return jwt.sign(payload, privateKey, {
    issuer: 'BEBLOG',
    subject: 'quockidang@gmail.com',
    audience: 'quockidang@gmail.com',
    expiresIn: '7d',
  });
};

const verifyAccessToken = (token) => {
  return jwt.verify(token, privateKey, {
    issuer: 'BEBLOG',
    subject: 'quockidang@gmail.com',
    audience: 'quockidang@gmail.com',
    expiresIn: '120s',
  });
};

const verifyRefreshToken = (token) => {
  return jwt.verify(token, privateKey, {
    issuer: 'BEBLOG',
    subject: 'quockidang@gmail.com',
    audience: 'quockidang@gmail.com',
    expiresIn: '7d',
  });
};

const decode = (token) => {
  return jwt.decode(token, { complete: true });
};

export default {
  signAccessToken,
  signRefreshToken,
  verifyAccessToken,
  verifyRefreshToken,
  decode,
};
