import bcrypt from 'bcryptjs';
import uniqid from 'uniqid';

class Utils {
  // make password
  static makePassword(pass) {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10, (err, salt) => {
        if (err !== null) {
          reject({
            message: 'error when make default password',
          });
        }

        bcrypt.hash(pass, salt, (error, hash) => {
          if (error !== null) {
            reject({
              message: 'error when make default password',
            });
          }
          resolve(hash);
        });
      });
    });
  }

  // make randomToken
  static randomToken() {
    return uniqid();
  }

  static async pagination(schema, page, pageSize) {
    let query = Object.create(schema);

    page = parseInt(page);
    pageSize = parseInt(pageSize);
    const skip = (page - 1) * pageSize;
    const total_items = await query.countDocuments();
    return new Promise((resolve, reject) => {
      schema
        .skip(skip)
        .limit(pageSize)
        .exec(function (err, items) {
          if (err) reject(err);

          const total_pages = Math.ceil(total_items / pageSize);
          resolve({
            items,
            current: page,
            before: page - 1 == 0 ? page : page - 1,
            total_pages,
            total_items,
            last: total_pages,
            next: total_pages == page ? page : page + 1,
          });
        });
    });
  }

  static async comparePassword(password, hashedPassword) {
    try {
      return await bcrypt.compare(password, hashedPassword);
    } catch (err) {
      return err;
    }
  }
}

export default Utils;
