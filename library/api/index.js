import StatusCode from '../../configs/StatusCode.js';
import Constant from '../../configs/Constant.js';
import ErrorMessage from '../../configs/ErrorMessage.js';
export const outputSuccess = (req, res, data) => {
  return res.status(StatusCode.SUCCESS).json({
    code: StatusCode.SUCCESS,
    msg: Constant.MESSAGE_CONTENT_DEFAULT,
    data,
  });
};

export const outputFile = (req, res) => {};

export const errorRequest = (req, res, msg) => {
  return res.status(StatusCode.BAD_REQUEST).json({
    code: StatusCode.BAD_REQUEST,
    msg,
    data: [],
  });
};

export const unAuthorized = (req, res) => {
  return res.status(StatusCode.UNAUTH).json({
    code: StatusCode.UNAUTH,
    msg: ErrorMessage.UNAUTHORIZED,
    data: [],
  });
};
