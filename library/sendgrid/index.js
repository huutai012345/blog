import sgMail from '@sendgrid/mail';

const send = async (email, subject, text, html) => {
  const msg = {
    to: email,
    from: 'nhtai0123456@gmail.com',
    subject,
    text,
    html,
  };
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  await sgMail.send(msg);
};

export default {
  send,
};
